" This vimrc is still growing.
" It uses default coloring, in combination 
" with a custom Terminal theme for Mac OSX.
"
" For any questions feel free to email me
"	timon.link@gmail.com
 

" Basic Setup
set expandtab
set shiftwidth=4
set softtabstop=4
set autoindent
set smartindent
set cindent
set number
set cursorline

" Coloring settings
syn on
hi LineNr ctermbg=DarkGray
hi LineNr ctermfg=Green
hi CursorLine ctermbg=None
 
nnoremap <Leader>c :set cursorline!

" Plugin Settings



